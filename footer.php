<div class="portal-footer">
			<div class="container">
				<div class="container-small">
					<div class="row">
						<div class="col-xs-6 col-sm-4 align-center">
							<div class="row portal-footer-icon border-right">
								<img class="img-part-1" src="<?=bloginfo('template_url')?>/assets/versi3/logo_white.png"/>
								<img class="img-part-1" src="<?=bloginfo('template_url')?>/assets/versi3/logo_ITB.png"/>
							</div>
						</div>
						<div class="col-xs-6 col-sm-3 portal-footer-menu align-center border-right">
							<ul>
								<li>Search</li>
								<li>FAQ</li>
								<li>Site Map</li>
								<li>Contacts</li>
							</ul>
							<!-- <div class="row portal-footer-menu">
								<div class="col-sm-2 col-sm-offset-1">
									<ul>
										<li>Home</li>
										<li>About</li>
										<li>Profile</li>
									</ul>
								</div>
								<div class="col-sm-3">
									<ul>
										<li>Fundraising</li>
										<li>How to Fundraising</li>
										<li>Schedule</li>
									</ul>
								</div>
								<div class="col-sm-3">
									<ul>
										<li>Event</li>
										<li>Guest Lists</li>
										<li>More</li>
									</ul>
								</div>
							</div> -->
						</div>
						<div class="col-xs-6 col-sm-5 portal-footer-menu">
							<ul>
								<li>Sekretariat</li>
								<li>Jln. Ganeca 10, Bandung, 40xxx</li>
								<li>Telp. 0856 xxxx 0989 Fax. 022 - 829 xx xx</li>
								<li>email@alamatemail.com</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


	
	<script type="text/javascript">
		$(window).load(function(){
			var squareElWidth = $(".square");
		    $.each(squareElWidth,function() {
		        var elWidth = $(this).width();
		        $(this).height(elWidth);
		    });
		});	
		$(window).resize(function(){
			var squareElWidth = $(".square");
		    $.each(squareElWidth,function() {
		        var elWidth = $(this).width();
		        $(this).height(elWidth);
		    });
		});
	</script>
	<script type="text/javascript">
		$(window).load(function(){
			var fullHeightEl = $('.full-height-image');

		    $.each(fullHeightEl,function() {
		        var height = $(this).parent().parent().height();
		        $(this).height(height);
		    });
		});
	</script>
	<script type="text/javascript">
	    $('.dropdown-menu').click(function(e) {
	        e.stopPropagation();
	    });
	</script>
</body>
</html>


<!-- http://www.cssscript.com/create-a-multi-level-drop-down-menu-with-pure-css/ -->
<html>
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <style>
    /* Prevents slides from flashing */
    #slides {
      display:none;
    }
  </style>

    <title>HOMECOMING ITB</title>
    <!-- Bootstrap Core CSS -->
    <link href="<?=bloginfo('template_url')?>/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <style type="text/css">
    	@font-face {
		  font-family: 'Trebuchet MS';
		  font-style: normal;
		  font-weight: 400;
		  src: local('Trebuchet MS'), local('TrebuchetMS'), url(http://fonts.gstatic.com/l/font?kit=9JQXGMjdcHmXcaI1rQ5rb_k_vArhqVIZ0nv9q090hN8&skey=7e071cef4f2cf8ce) format('woff2');
		}
		@font-face {
		  font-family: 'Trebuchet MS';
		  font-style: normal;
		  font-weight: 700;
		  src: local('Trebuchet MS Bold'), local('TrebuchetMS-Bold'), url(http://fonts.gstatic.com/l/font?kit=5ozLdgd-UkdFLWa7JSF1-rtgMuu8PP30JY7uijDhKnE&skey=3f0797581a987e40) format('woff2');
		}
		@font-face {
		  font-family: 'Trebuchet MS';
		  font-style: italic;
		  font-weight: 400;
		  src: local('Trebuchet MS Italic'), local('TrebuchetMS-Italic'), url(http://fonts.gstatic.com/l/font?kit=lNFXl5wenEwSNlIU9g-Z3B8g4Wdd7hYU3Jvl4elC6n4&skey=a5a066eb747abea4) format('woff2');
		}
		@font-face {
		  font-family: 'Trebuchet MS';
		  font-style: italic;
		  font-weight: 700;
		  src: local('Trebuchet MS Bold Italic'), local('Trebuchet-BoldItalic'), url(http://fonts.gstatic.com/l/font?kit=d8kxGwlaqsR0T3Chlg-X6mceCCgISh9AIMdEVQO6ryk&skey=8ff479c3862a8342) format('woff2');
		}
		body {
		    font-family: 'Trebuchet Ms';
		    font-size: 200%;
		}
    </style>

    <link href="<?=bloginfo('template_url')?>/style.css" rel="stylesheet">
    <link href="<?=bloginfo('template_url')?>/css/owl.carousel.css" rel="stylesheet">
    <link href="<?=bloginfo('template_url')?>/css/owl.theme.css" rel="stylesheet">
    <script src="<?=bloginfo('template_url')?>/js/jquery-1.11.3.min.js"></script>
    <script src="<?=bloginfo('template_url')?>/js/bootstrap.min.js"></script>
    <script src="<?=bloginfo('template_url')?>/js/owl.carousel.js"></script>
    <script type="text/javascript">
    	function scrollToAnchor(offset){
		    //var aTag = $(aid);
		    $('html,body').animate({scrollTop: offset},'slow');
		}
    </script>
    <script src="http://maps.googleapis.com/maps/api/js"></script>
    <script>
	function initialize() {
	  var mapProp = {
	    center:new google.maps.LatLng(-6.8920,107.6100),
	    zoom:15,
	    mapTypeId:google.maps.MapTypeId.ROADMAP
	  };
	  var map=new google.maps.Map(document.getElementById("googleMap"), mapProp);
	}
	google.maps.event.addDomListener(window, 'load', initialize);
	</script>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
	<div class="fluid-container page-wrapper">
		<div class="container">
			<div class="container-small">
				<div class="portal-header">
					<div class="header-main">
						<div class="row">
							<div class="col-sm-2">
								<img class="display-inline" src="<?=bloginfo('template_url')?>/assets/versi3/header_logo.png"/>
							</div>
							<div class="col-sm-10">
								<ul class="display-inline">
									<li class="active">Home</li>
									<li>Schedule</li>
									<li>Events</li>
									<li>Fundrasing</li>
									<li>Perkembangan ITB</li>
									<li>Apa Kata Mereka</li>
									<li>Info</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="header-secondary">
						<ul class="display-inline">
							<li>UMUM</li>|						
							<li>FMIPA</li>|
							<li>SITH</li>|
							<li>SF</li>|
							<li>FTTM</li>|
							<li>FITB</li>|
							<li>FTI</li>|					
						<!-- </ul> -->
						<!-- <ul class="display-inline"> -->
							<li>STEI</li>|
							<li>FTMD</li>|
							<li>FTSL</li>|						
							<li>SAPPK</li>|
							<li>FSRD</li>|
							<li>SBM</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="portal-content">
			<div class="container">
				<div class="container-small">
					<div id="carousel" class="owl-carousel">
						<div><img src="<?=bloginfo('template_url')?>/assets/versi3/Taman Aula Barat 1.png"/></div>
						<div><img src="<?=bloginfo('template_url')?>/assets/versi3/Taman Aula Barat 1.png"/></div>
						<div><img src="<?=bloginfo('template_url')?>/assets/versi3/Taman Aula Barat 1.png"/></div>
						<div><img src="<?=bloginfo('template_url')?>/assets/versi3/Taman Aula Barat 1.png"/></div>
					</div>

					<h3 class="h-style">
						Recent Project	
					</h3>
					<div class="box">
						<div class="row">
							<div class="col-sm-4">
								<div class="square" style="background:url('<?=bloginfo('template_url')?>/assets/versi3/indBG.jpg'); background-size:cover; background-position: center;"></div>
								<h4 class="h-style">Pasar Seni</h4>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
								tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
								quis nostrud exercitation ullamco laboris nisi ut.</p>
							</div>
							<div class="col-sm-4">
								<div class="square" style="background:url('<?=bloginfo('template_url')?>/assets/versi3/indBG.jpg'); background-size:cover; background-position: center;"></div>
								<h4 class="h-style">Pasar Seni</h4>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
								tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
								quis nostrud exercitation ullamco laboris nisi ut.</p>
							</div>
							<div class="col-sm-4">
								<div class="square" style="background:url('<?=bloginfo('template_url')?>/assets/versi3/indBG.jpg'); background-size:cover; background-position: center;"></div>
								<h4 class="h-style">Pasar Seni</h4>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
								tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
								quis nostrud exercitation ullamco laboris nisi ut.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="portal-footer">
			<div class="container">
				<div class="container-small">
					<div class="row">
						<div class="col-sm-4">
							<div class="row portal-footer-icon ">
								<img class="img-part-1" src="<?=bloginfo('template_url')?>/assets/versi3/logo_white.png"/>
								<img class="img-part-1" src="<?=bloginfo('template_url')?>/assets/versi3/logo_ITB.png"/>
							</div>
						</div>
						<div class="col-sm-3 portal-footer-menu align-center border-right">
							<ul>
								<li>Search</li>
								<li>FAQ</li>
								<li>Site Map</li>
								<li>Contacts</li>
							</ul>
							<!-- <div class="row portal-footer-menu">
								<div class="col-sm-2 col-sm-offset-1">
									<ul>
										<li>Home</li>
										<li>About</li>
										<li>Profile</li>
									</ul>
								</div>
								<div class="col-sm-3">
									<ul>
										<li>Fundraising</li>
										<li>How to Fundraising</li>
										<li>Schedule</li>
									</ul>
								</div>
								<div class="col-sm-3">
									<ul>
										<li>Event</li>
										<li>Guest Lists</li>
										<li>More</li>
									</ul>
								</div>
							</div> -->
						</div>
						<div class="col-sm-5 portal-footer-menu">
							<ul>
								<li>Sekretariat</li>
								<li>Jln. Ganeca 10, Bandung, 40xxx</li>
								<li>Telp. 0856 xxxx 0989 Fax. 022 - 829 xx xx</li>
								<li>email@alamatemail.com</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

<!-- modal -->
	<div id="email-modal" class="modal fade" role="dialog">
	  <div class="modal-dialog email-modal-wrapper">
		<button type="button" class="close" data-dismiss="modal">&times;</button>
		<div class="email-modal-content">
			<img class="img-part-1" src="<?=bloginfo('template_url')?>/assets/versi3/email_icon.png"/>
			<hr>
			<p>Masukkan email Anda dan berlangganan news letter dari kami untuk mendapatkan kabar terbaru mengenai ITB Homecoming Alumni 2015.</p>
			<div class="inner-addon right-addon input-email">
			 	<a href=""><span class="glyphicon glyphicon-play"></span></a>
			    <input type="text" class="form-control" placeholder="Email Anda" />
			</div>
		</div>
	  </div>
	</div>

	<!-- <div class="container">
		<div id="part1" class="part1">
			<div class="part1content--1">
	        	<img class="img1" src="<?=bloginfo('template_url')?>/assets/board1/logo_white.png"/>
	        </div>
	        <div class="part1content--2">
	        	Sekali teman, tetap teman
	        </div>
	        <div class="part1content--3">
	        	7 November 2015
	        </div>
	        <img src="<?=bloginfo('template_url')?>/assets/atas.jpg"/>
		</div>
		<div id="part2" class="part2">
			<div class="part2content--1">
	        	<img class="img2" src="<?=bloginfo('template_url')?>/assets/board3/Board 3-05.png"/>	
	        </div>
	        <div class="part2content--2">
	        	Mewadahi dan menjembatani seluruh alumni tanpa batas untuk turut serta membangun dan mengembangkan kampus ITB
	        </div>	
	        <div class="part2content--3">
	        	<img class="img3" src="<?=bloginfo('template_url')?>/assets/board2/Board 2-04.png"/>	
	        </div>
	        <div class="part2content--4">
	        	Sebuah dukungan untuk mewujudkan ITB sebagai World-Class University dan Entrepreneural University
	        </div>
			<img src="<?=bloginfo('template_url')?>/assets/tengah.jpg"/>	
		</div>
		<div class="part3">
			<div class="part3content--1">
	        	<img src="<?=bloginfo('template_url')?>/assets/board2/Board 2-14.png"/>	
	        </div>
	        <div class="part3content--2">
	        	Membuka pintu lebar kepulangan seluruh alumni untuk bernostalgia di kampus melalui berbagai acara kumpul dan acara-acara kesenian
	        </div>
	        <div id="googleMap">
	        </div>
	        <div class="part3content--3">
	        	Informasi lebih lanjut:<br>
	        	HUMAS ITB <br><blue>+62 812 2090 131</blue><br>
	        	SEKRETARIAT PRODI STEI <blue><br>+62 22 250 8135 / 250 8136</blue>
	        </div>
			<img src="<?=bloginfo('template_url')?>/assets/bawah.jpg"/>	
		</div>
	</div> -->
	<script>
		$(document).ready(function() {
			$("#carousel").owlCarousel({
				slideSpeed : 300,
				paginationSpeed : 400,
				autoPlay : true,
				singleItem : true
			});
		});
	</script>
	<script type="text/javascript">
		$(function() {    
		var menuSlider = $('.menu-slider');

		$('.hamburger-menu').click(function(e) {
			e.preventDefault();
			if(!menuSlider.hasClass('active')) {
				menuSlider.addClass('active');
			} else {
				menuSlider.removeClass('active');
			}
		}); 
	});
	</script>
	<script>
		// $(window).load(function(){
		// 	var widthBG = $('.main-index').width();
		// 	var heightBG = (1728/1600) * widthBG ;
		// 	$('.main-index').css({'height':heightBG +'px'});
		// });
		// $(window).resize(function(){
		// 	var widthBG = $('.main-index').width();
		// 	var heightBG = (1728/1600) * widthBG ;
		// 	$('.main-index').css({'height':heightBG +'px'});
		// });
	</script>
	<script>
		var wid = $('.block1').width();
		var lef = wid/2;
		$( document ).ready(function() {
		    $(".block1").animate({ width: "0", left: "+="+lef }, 2000);
		});
	</script>
	<script type="text/javascript">
		var lastScrollTop = 0;
		var currentAnchor = 0;
		$(window).scroll(function() {
			if ($(this).scrollTop() > 150) {
		    	$(".img2").slideDown();
		    }
		    if ($(this).scrollTop() < 150) {
		    	$(".img2").slideUp();
		    }
		    if ($(this).scrollTop() > 200) {
		    	$(".part2content--2").slideDown();
		    }
		    if ($(this).scrollTop() < 200) {
		    	$(".part2content--2").slideUp();
		    }
		    if ($(this).scrollTop() > 400) {
		    	$(".img3").slideDown();
		    }
		    if ($(this).scrollTop() < 400) {
		    	$(".img3").slideUp();
		    }
		    if ($(this).scrollTop() > 500) {
		    	$(".part2content--4").slideDown();
		    }
		    if ($(this).scrollTop() < 500) {
		    	$(".part2content--4").slideUp();
		    }
		});
		    
	</script>
	<!-- square div -->
	<script type="text/javascript">
		$(window).load(function(){
			var squareElWidth = $(".square");
		    $.each(squareElWidth,function() {
		        var elWidth = $(this).width();
		        $(this).height(elWidth);
		    });
		});	
		$(window).resize(function(){
			var squareElWidth = $(".square");
		    $.each(squareElWidth,function() {
		        var elWidth = $(this).width();
		        $(this).height(elWidth);
		    });
		});
	</script>
	<script type="text/javascript">
		$(window).load(function(){
			var fullHeightEl = $('.full-height-image');

		    $.each(fullHeightEl,function() {
		        var height = $(this).parent().parent().height();
		        $(this).height(height);
		    });
		});
	</script>
</body>
</html>
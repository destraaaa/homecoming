<?php /* Template Name: Schedule */ ?>
<html>
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <style>
    /* Prevents slides from flashing */
    #slides {
      display:none;
    }
  </style>

    <title>HOMECOMING ITB</title>
    <!-- Bootstrap Core CSS -->
    <link href="<?=bloginfo('template_url')?>/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <style type="text/css">
    	@font-face {
		  font-family: 'Trebuchet MS';
		  font-style: normal;
		  font-weight: 400;
		  src: local('Trebuchet MS'), local('TrebuchetMS'), url(http://fonts.gstatic.com/l/font?kit=9JQXGMjdcHmXcaI1rQ5rb_k_vArhqVIZ0nv9q090hN8&skey=7e071cef4f2cf8ce) format('woff2');
		}
		@font-face {
		  font-family: 'Trebuchet MS';
		  font-style: normal;
		  font-weight: 700;
		  src: local('Trebuchet MS Bold'), local('TrebuchetMS-Bold'), url(http://fonts.gstatic.com/l/font?kit=5ozLdgd-UkdFLWa7JSF1-rtgMuu8PP30JY7uijDhKnE&skey=3f0797581a987e40) format('woff2');
		}
		@font-face {
		  font-family: 'Trebuchet MS';
		  font-style: italic;
		  font-weight: 400;
		  src: local('Trebuchet MS Italic'), local('TrebuchetMS-Italic'), url(http://fonts.gstatic.com/l/font?kit=lNFXl5wenEwSNlIU9g-Z3B8g4Wdd7hYU3Jvl4elC6n4&skey=a5a066eb747abea4) format('woff2');
		}
		@font-face {
		  font-family: 'Trebuchet MS';
		  font-style: italic;
		  font-weight: 700;
		  src: local('Trebuchet MS Bold Italic'), local('Trebuchet-BoldItalic'), url(http://fonts.gstatic.com/l/font?kit=d8kxGwlaqsR0T3Chlg-X6mceCCgISh9AIMdEVQO6ryk&skey=8ff479c3862a8342) format('woff2');
		}
		body {
		    font-family: 'Trebuchet Ms';
		    font-size: 200%;
		}
    </style>

    <link href="<?=bloginfo('template_url')?>/style.css" rel="stylesheet">
    <script src="<?=bloginfo('template_url')?>/js/jquery-1.11.3.min.js"></script>
    <script src="<?=bloginfo('template_url')?>/js/bootstrap.min.js"></script>
    <script type="text/javascript">
    	function scrollToAnchor(offset){
		    //var aTag = $(aid);
		    $('html,body').animate({scrollTop: offset},'slow');
		}
    </script>
    <script src="http://maps.googleapis.com/maps/api/js"></script>
    <script>
	function initialize() {
	  var mapProp = {
	    center:new google.maps.LatLng(-6.8920,107.6100),
	    zoom:15,
	    mapTypeId:google.maps.MapTypeId.ROADMAP
	  };
	  var map=new google.maps.Map(document.getElementById("googleMap"), mapProp);
	}
	google.maps.event.addDomListener(window, 'load', initialize);
	</script>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
	<div class="container">
		<div id="part1" class="part1">
			<div class="part1content--1">
	        	<img class="img1" src="<?=bloginfo('template_url')?>/assets/board1/logo_white.png"/>
	        </div>
	        <div class="part1content--2">
	        	Sekali teman, tetap teman
	        </div>
	        <div class="part1content--3">
	        	7 November 2015
	        </div>
	        <img src="<?=bloginfo('template_url')?>/assets/atas.jpg"/>
		</div>
		<div class="part2 full-height">
		<div class="title-schedule">
			<h1 style="text-align:center">Schedule</h1>
		</div>
			<?php
				$args = array (
								'post_type' => 'post',
								'post_status' => 'publish',
								'showposts' => -1,
								'category_name' => 'Schedule',
								'order' => 'ASC'
							);
				$my_query = null;
				$my_query = new WP_Query($args);
			?>
			<?php if ( $my_query->have_posts() ) : while ( $my_query->have_posts() ) : $my_query->the_post(); ?>
			<?php
				$content = get_the_content();
				$row_items = explode(',', $content);
			?>
			<div class="row">
				<div class="col-md-1">
					<?php echo get_the_title(); ?>
				</div>
				<div class="col-md-11">
				<?php for($i = 0; $i<sizeof($row_items); $i++) { ?>
					<?php
						$detail = explode('-', $row_items[$i]);
					?>
					<div class="row schedule-item">
						<div class="col-md-1">
							<?php echo $detail[0]; ?><br>
							<?php echo $detail[1]; ?>
						</div>
						<div class="col-md-8" style="border-left: solid <?php echo $detail[5]; ?> 3px">
							<?php echo $detail[2]; ?><br>
							<span style="color: <?php echo $detail[5]; ?>"><?php echo $detail[4]; ?></span>
						</div>
						<div class="col-md-3">
							<?php echo $detail[3]; ?><br>
						</div>
					</div>
				<?php } ?>
				</div>
			</div>
			<?php endwhile; else : ?>
				<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
			<?php endif; ?>
		</div>
	</div>
</body>
</html>
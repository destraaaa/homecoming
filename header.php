<html>
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <style>
    /* Prevents slides from flashing */
    #slides {
      display:none;
    }
  </style>

    <title>HOMECOMING ITB</title>
    <!-- Bootstrap Core CSS -->
    <link href="<?=bloginfo('template_url')?>/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <style type="text/css">
    	@font-face {
		  font-family: 'Trebuchet MS';
		  font-style: normal;
		  font-weight: 400;
		  src: local('Trebuchet MS'), local('TrebuchetMS'), url(http://fonts.gstatic.com/l/font?kit=9JQXGMjdcHmXcaI1rQ5rb_k_vArhqVIZ0nv9q090hN8&skey=7e071cef4f2cf8ce) format('woff2');
		}
		@font-face {
		  font-family: 'Trebuchet MS';
		  font-style: normal;
		  font-weight: 700;
		  src: local('Trebuchet MS Bold'), local('TrebuchetMS-Bold'), url(http://fonts.gstatic.com/l/font?kit=5ozLdgd-UkdFLWa7JSF1-rtgMuu8PP30JY7uijDhKnE&skey=3f0797581a987e40) format('woff2');
		}
		@font-face {
		  font-family: 'Trebuchet MS';
		  font-style: italic;
		  font-weight: 400;
		  src: local('Trebuchet MS Italic'), local('TrebuchetMS-Italic'), url(http://fonts.gstatic.com/l/font?kit=lNFXl5wenEwSNlIU9g-Z3B8g4Wdd7hYU3Jvl4elC6n4&skey=a5a066eb747abea4) format('woff2');
		}
		@font-face {
		  font-family: 'Trebuchet MS';
		  font-style: italic;
		  font-weight: 700;
		  src: local('Trebuchet MS Bold Italic'), local('Trebuchet-BoldItalic'), url(http://fonts.gstatic.com/l/font?kit=d8kxGwlaqsR0T3Chlg-X6mceCCgISh9AIMdEVQO6ryk&skey=8ff479c3862a8342) format('woff2');
		}
		body {
		    font-family: 'Trebuchet Ms';
		    font-size: 200%;
		}
    </style>

    <link href="<?=bloginfo('template_url')?>/style.css" rel="stylesheet">
    <link href="<?=bloginfo('template_url')?>/css/owl.carousel.css" rel="stylesheet">
    <link href="<?=bloginfo('template_url')?>/css/owl.theme.css" rel="stylesheet">
    <script src="<?=bloginfo('template_url')?>/js/jquery-1.11.3.min.js"></script>
    <script src="<?=bloginfo('template_url')?>/js/bootstrap.min.js"></script>
    <script src="<?=bloginfo('template_url')?>/js/owl.carousel.js"></script>
    <script type="text/javascript">
    	function scrollToAnchor(offset){
		    //var aTag = $(aid);
		    $('html,body').animate({scrollTop: offset},'slow');
		}
    </script>
    <script src="http://maps.googleapis.com/maps/api/js"></script>
    <script>
	function initialize() {
	  var mapProp = {
	    center:new google.maps.LatLng(-6.8920,107.6100),
	    zoom:15,
	    mapTypeId:google.maps.MapTypeId.ROADMAP
	  };
	  var map=new google.maps.Map(document.getElementById("googleMap"), mapProp);
	}
	google.maps.event.addDomListener(window, 'load', initialize);
	</script>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
	<div class="page-wrapper">
		<div class="container">
			<div class="container-small">
				<div class="portal-header">
					<div class="header-main">
						<div class="row">
							<div class="col-sm-12">
								<div class="search-box">
									<div class="inner-addon right-addon">
									    <i class="glyphicon glyphicon-play"></i>
									    <input type="text" class="form-control" placeholder="Masukkan Pencarian" />
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-2 align-center">
								<img class="display-inline" src="<?=bloginfo('template_url')?>/assets/versi3/header_logo.png"/>
							</div>
							<div class="col-sm-10">
								<ul class="display-inline">
									<li class="active">Home</li>
									<li>Schedule</li>
									<li>Events</li>
									<li>Fundrasing</li>
									<li>Perkembangan ITB</li>
									<li>Apa Kata Mereka</li>
									<li>Info</li>
								</ul>
							</div>
						</div>
							
					</div>
					<div class="header-secondary">
						<h1 class="display-inline-block"><b>Fakultas Seni Rupa dan Desain</b></h1>
								<span class="dropdown-toggle display-inline" id="dropdown-fakultas" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
									&#9660;
								</span>
								<div class="dropdown-menu dropdown-fakultas" >
									<ul class="">
										<li>Umum
											<span class="pull-right arrow--transparent">&#9660;</span>
											<ul>
												<li>umum 1</li>
												<li>umum 2</li>
												<li>umum 3</li>
											</ul>
										</li>
										<li>Fakultas Matematika dan Ilmu Pengetahuan Alam
											<span class="pull-right arrow--transparent">&#9660;</span>
											<ul>
												<li>mipa 1</li>
												<li>mipa 2</li>
												<li>mipa 3</li>
											</ul>
										</li>

									</ul>
								</div>
						<!-- <h1>
							<div class="dropdown">
								<b>Fakultas Seni Rupa dan Desain</b>
								<span class="dropdown-toggle" id="dropdown-fakultas" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
									&#9660;
								</span>
								<div class="dropdown-menu dropdown-fakultas" >
									<ul>
										<li>Umum
											<span class="pull-right arrow--transparent">&#9660;</span>
										</li>
										<li>Fakultas Matematika dan Ilmu Pengetahuan Alam
											<span class="pull-right arrow--transparent">&#9660;</span>
										</li>
										<li>Sekolah Farmasi
											<span class="pull-right arrow--transparent">&#9660;</span>
										</li>
										<li>Fakultas Teknik Pertambangan dan Perminyakan
											<span class="pull-right arrow--transparent">&#9660;</span>
										</li>
										<li>Fakultas Ilmu Tanah dan Kebumian
											<span class="pull-right arrow--transparent">&#9660;</span>
										</li>
										<li>Fakultas Teknik Industri
											<span class="pull-right arrow--transparent">&#9660;</span>
										</li>
										<li>Sekolah Teknik Elektro dan Informatika
											<span class="pull-right arrow--transparent">&#9660;</span>
										</li>
										<li>Fakultas Teknik Mesin dan Dirgantara
											<span class="pull-right arrow--transparent">&#9660;</span>
										</li>
										<li>Fakultas Teknik Sipil dan Lingkungan
											<span class="pull-right arrow--transparent">&#9660;</span>
										</li>
										<li>Sekolah Arsitektur, Perencanaan dan Pengembangan Kebijakan
											<span class="pull-right arrow--transparent">&#9660;</span>
										</li>
										<li>Fakultas Seni Rupa dan Desain
											<span class="pull-right arrow--transparent dropdown-toggle" id="dropdown-fsrd" data-toggle="dropdown" aria-expanded="true">&#9660;</span>
											<div class="dropdown-menu">
												<ul>
													<li>Desain Komunikasi Visual</li>
													<li>Desain Interior</li>
													<li>Desain Produk</li>
													<li>Kriya</li>
													<li>Seni Rupa</li>
												</ul>
											</div>
										<li>Sekolah Bisnis dan Manajemen
											<span class="pull-right arrow--transparent">&#9660;</span>
										</li>

									</ul>
								</div>
							</div>						
						</h1> -->
						<div class="list-prodi">
							<ul>
								<li class="active">Desain Komunikasi Visual</li>|
								<li>Desain Interior</li>|
								<li>Desain Produk</li>|
								<li>Kriya</li>|
								<li>Seni Murni</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
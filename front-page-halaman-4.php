<?php get_header();?>
		<div class="portal-content">
			<div class="container">
				<div class="container-small">
					<div class="row">
						<div class="col-sm-9">
							<div class="box">
								<img class="img-part-1" src="<?=bloginfo('template_url')?>/assets/versi3/indBG.jpg"/>
								<div class="post-title h-style">
									PEMBANGUNAN TAMAN AULA BARAT
								</div>
								<div class="post-creator">
									by : UMUM
								</div>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
								tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
								quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
								consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
								cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
								proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
								<div class="padding">
									<img class="img-part-1" src="<?=bloginfo('template_url')?>/assets/versi3/indBG.jpg"/>
								</div>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
								tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
								quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
								consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
								cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
								proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

							</div>
						</div>
						<div class="col-sm-3">
							<div class="box project-detail-info">
								<div class="statistic">
									<div class="number">
										10,000
									</div>
									<div class="label">
										target
									</div>
								</div>
								<div class="statistic">
									<div class="number">
										80
									</div>
									<div class="label">
										donatur
									</div>
								</div>
								<div class="statistic">
									<div class="number">
										9
									</div>
									<div class="label">
										hari lagi
									</div>
								</div>
								<div class="progress post-progress">
								  <div class="progress-bar" role="progressbar" aria-valuenow="70"
								  aria-valuemin="0" aria-valuemax="100" style="width:18%">
								  </div>
								</div>
								<a href="" data-toggle="modal" data-target="#ModalDonasi">
									<div class="donate-now">
										Donasi Sekarang
									</div>
								</a>
							</div>
							<div class="paket">
								<div class="main-title">
									Pilih Paket Donasi:
								</div>
								<div class="panel-paket">
									<div class="row">
										<div class="col-sm-6">
											<div class="title" style="color:red;">
												Paket I
											</div>
										</div>
										<div class="col-sm-6 align-right">
											<div class="quota">
												12 Paket Lagi
											</div>
										</div>
									</div>
									<div class="backers">
										70 Person
									</div>
									<div class="reward">
										Reward:
										<br>
										Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
										tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
										quis nostrud exercitation ulest laborum.
									</div>
								</div>
								<div class="panel-paket">
									<div class="row">
										<div class="col-sm-6">
											<div class="title" style="color:red;">
												Paket I
											</div>
										</div>
										<div class="col-sm-6 align-right">
											<div class="quota">
												12 Paket Lagi
											</div>
										</div>
									</div>
									<div class="backers">
										70 Person
									</div>
									<div class="reward">
										Reward:
										<br>
										Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
										tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
										quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
										consequat. Duis aute irure dolor in rlit anim id est laborum.
									</div>
								</div>
								<div class="panel-paket">
									<div class="row">
										<div class="col-sm-6">
											<div class="title" style="color:red;">
												Paket I
											</div>
										</div>
										<div class="col-sm-6 align-right">
											<div class="quota">
												12 Paket Lagi
											</div>
										</div>
									</div>
									<div class="backers">
										70 Person
									</div>
									<div class="reward">
										Reward:
										<br>
										Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
										tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
										quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
										consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
										cillum dolore eu fugiat nulla pariatum.
									</div>
								</div>
							</div>
						</div>
					</div>
					
					<div class="dotted-line"></div>
					<h3 class="h-style">
						Recent View Project	
					</h3>
					<div class="box">
						<div class="row">
							<div class="col-sm-4">
								<div class="square" style="background:url('<?=bloginfo('template_url')?>/assets/versi3/indBG.jpg'); background-size:cover; background-position: center;"></div>
								<h4 class="h-style">Pasar Seni</h4>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
								tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
								quis nostrud exercitation ullamco laboris nisi ut.</p>
							</div>
							<div class="col-sm-4">
								<div class="square" style="background:url('<?=bloginfo('template_url')?>/assets/versi3/indBG.jpg'); background-size:cover; background-position: center;"></div>
								<h4 class="h-style">Pasar Seni</h4>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
								tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
								quis nostrud exercitation ullamco laboris nisi ut.</p>
							</div>
							<div class="col-sm-4">
								<div class="square" style="background:url('<?=bloginfo('template_url')?>/assets/versi3/indBG.jpg'); background-size:cover; background-position: center;"></div>
								<h4 class="h-style">Pasar Seni</h4>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
								tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
								quis nostrud exercitation ullamcodasdasdasconsectetur adipisicing elit, sed do eiusmod
								tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
								quis nostrud exercitation ullamcodasdasdas laboris nisi ut.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
<?php get_footer(); ?>



<div id="ModalDonasi" class="modal fade " tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	  <div class="modal-dialog ">
		<div class="modal-content modal-dark modal-donasi">
			<h3 class="title h-style">
				PEMBANGUNAN TAMAN AULA BARAT
			</h3>
			<div class="by">
				by : UMUM
			</div>
			<p>Pilih Paket Donasi:</p>
			<div class="row paket">
				<div class="col-sm-4">
					<div class="panel-paket">
						<div class="row">
							<div class="col-sm-6">
								<div class="title" style="color:red;">
									Paket I
								</div>
							</div>
							<div class="col-sm-6 align-right">
								<div class="quota">
									12 Paket Lagi
								</div>
							</div>
						</div>
						<div class="backers">
							70 Person
						</div>
						<div class="reward">
							Reward:
							<br>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
							tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
							quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
							consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
							cillum dolore eu fugiat nulla pariatum.
						</div>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="panel-paket">
						<div class="row">
							<div class="col-sm-6">
								<div class="title" style="color:red;">
									Paket I
								</div>
							</div>
							<div class="col-sm-6 align-right">
								<div class="quota">
									12 Paket Lagi
								</div>
							</div>
						</div>
						<div class="backers">
							70 Person
						</div>
						<div class="reward">
							Reward:
							<br>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
							tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
							quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
							consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
							cillum dolore eu fugiat nulla pariatum.
						</div>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="panel-paket">
						<div class="row">
							<div class="col-sm-6">
								<div class="title" style="color:red;">
									Paket I
								</div>
							</div>
							<div class="col-sm-6 align-right">
								<div class="quota">
									12 Paket Lagi
								</div>
							</div>
						</div>
						<div class="backers">
							70 Person
						</div>
						<div class="reward">
							Reward:
							<br>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
							tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
							quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
							consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
							cillum dolore eu fugiat nulla pariatum.
						</div>
					</div>
				</div>
			</div>
		</div>
	  </div>
	</div>

<html>
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <style>
    /* Prevents slides from flashing */
    #slides {
      display:none;
    }
  </style>

    <title>HOMECOMING ITB</title>
    <!-- Bootstrap Core CSS -->
    <link href="<?=bloginfo('template_url')?>/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <style type="text/css">
    	@font-face {
		  font-family: 'Trebuchet MS';
		  font-style: normal;
		  font-weight: 400;
		  src: local('Trebuchet MS'), local('TrebuchetMS'), url(http://fonts.gstatic.com/l/font?kit=9JQXGMjdcHmXcaI1rQ5rb_k_vArhqVIZ0nv9q090hN8&skey=7e071cef4f2cf8ce) format('woff2');
		}
		@font-face {
		  font-family: 'Trebuchet MS';
		  font-style: normal;
		  font-weight: 700;
		  src: local('Trebuchet MS Bold'), local('TrebuchetMS-Bold'), url(http://fonts.gstatic.com/l/font?kit=5ozLdgd-UkdFLWa7JSF1-rtgMuu8PP30JY7uijDhKnE&skey=3f0797581a987e40) format('woff2');
		}
		@font-face {
		  font-family: 'Trebuchet MS';
		  font-style: italic;
		  font-weight: 400;
		  src: local('Trebuchet MS Italic'), local('TrebuchetMS-Italic'), url(http://fonts.gstatic.com/l/font?kit=lNFXl5wenEwSNlIU9g-Z3B8g4Wdd7hYU3Jvl4elC6n4&skey=a5a066eb747abea4) format('woff2');
		}
		@font-face {
		  font-family: 'Trebuchet MS';
		  font-style: italic;
		  font-weight: 700;
		  src: local('Trebuchet MS Bold Italic'), local('Trebuchet-BoldItalic'), url(http://fonts.gstatic.com/l/font?kit=d8kxGwlaqsR0T3Chlg-X6mceCCgISh9AIMdEVQO6ryk&skey=8ff479c3862a8342) format('woff2');
		}
		body {
		    font-family: 'Trebuchet Ms';
		    font-size: 200%;
		}
    </style>

    <link href="<?=bloginfo('template_url')?>/style.css" rel="stylesheet">
    <script src="<?=bloginfo('template_url')?>/js/jquery-1.11.3.min.js"></script>
    <script src="<?=bloginfo('template_url')?>/js/bootstrap.min.js"></script>
    <script type="text/javascript">
    	function scrollToAnchor(offset){
		    //var aTag = $(aid);
		    $('html,body').animate({scrollTop: offset},'slow');
		}
    </script>
    <script src="http://maps.googleapis.com/maps/api/js"></script>
    <script>
	function initialize() {
	  var mapProp = {
	    center:new google.maps.LatLng(-6.8920,107.6100),
	    zoom:15,
	    mapTypeId:google.maps.MapTypeId.ROADMAP
	  };
	  var map=new google.maps.Map(document.getElementById("googleMap"), mapProp);
	}
	google.maps.event.addDomListener(window, 'load', initialize);
	</script>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
	<div class="fluid-container">
		<div class="menu-slider">
			<img class="" src="<?=bloginfo('template_url')?>/assets/versi3/logo_col (1).png"/>
			<ul>
				<hr size="pixels" class="menu-slider-hr">
				<li><a href="">Home</a></li>
				<li><a href="">Schedule</a></li>
				<li><a href="">Event</a></li>
				<li><a href="">Fundraising</a></li>
				<li><a href="">Perkembangan ITB</a></li>
				<li><a href="">Apa Kata Mereka</a></li>
				<li><a href="">Contact</li>
			</ul>
		</div>
		<div class="cover-page">
			<a>
				<div class="hamburger-menu">
					<img class="" src="<?=bloginfo('template_url')?>/assets/versi3/hamburger.png"/>
				</div>
			</a>
			<img class="cover-image" src="<?=bloginfo('template_url')?>/assets/versi3/title.png"/>
			<br>
			<a href="#"><img class="arrow-down" src="<?=bloginfo('template_url')?>/assets/versi3/ardown.png"/></a>
		</div>
		<div class="main-index" id="main">
			<div class="row">
				<div class="col-sm-6">
					<div class="part-1 part-1-left">
						<div class="subtitle">
							Sekali teman,<br>
							tetap teman.
						</div>
						<div class="description desc-1">
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
							tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
							quis nostrud
						</div>
						<div class="description desc-2">
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
							tempor incididunt ut labore et dolore magna aliqua.
						</div>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="part-1 part-1-right">
						<img class="img-part-1" src="<?=bloginfo('template_url')?>/assets/versi3/img1.png"/>
					</div> 
				</div>
			</div>
		</div>
<?php get_footer(); ?>



<div id="ModalDonasi" class="modal fade " tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	  <div class="modal-dialog ">
		<div class="modal-content modal-dark modal-donasi">
			<h3 class="modal-title h-style">
				PEMBANGUNAN TAMAN AULA BARAT
			</h3>
			<div class="by">
				by : UMUM
			</div>
			<p>Pilih Paket Donasi:</p>
			<div class="row paket">
				<div class="col-sm-4">
					<div class="panel-paket">
						<div class="row">
							<div class="col-sm-6">
								<div class="title" style="color:red;">
									Paket I
								</div>
							</div>
							<div class="col-sm-6 align-right">
								<div class="quota">
									12 Paket Lagi
								</div>
							</div>
						</div>
						<div class="description desc-1">
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
							tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,	
						</div>
						<div class="participate-button">
							<img src="<?=bloginfo('template_url')?>/assets/versi3/part_button.png"/>
						</div>
					</div>
				</div>
			</div>
			<div class="row row-3">
				<div class="col-sm-6">
					<div class="part-3 part-3-left">
						<div class="subtitle">
							Memberikan Alumni sebuah pengalaman bernostalgia
						</div>
						<div class="description desc-1">
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
							tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
							quis nostrud exercitation ullamco laboris nisi ut aliquip ex 
						</div>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="part-3 part-3-right">
						<div class="subtitle">
							Mendukung <br> berkembangnya <br> ITB sebagai Almamater
						</div>
						<div class="description desc-1">
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
							tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,	
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="footer">
			<div class="row">
				<div class="col-sm-6">
					<div class="row footer-icon">
						<div class="col-sm-3 col-sm-offset-6">
							<img class="img-part-1" src="<?=bloginfo('template_url')?>/assets/versi3/logo_white.png"/>
						</div>
						<div class="col-sm-3">
							<img class="img-part-1" src="<?=bloginfo('template_url')?>/assets/versi3/logo_ITB.png"/>
						</div>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="row footer-menu">
						<div class="col-sm-2 col-sm-offset-1">
							<ul>
								<li>Home</li>
								<li>About</li>
								<li>Profile</li>
							</ul>
						</div>
						<div class="col-sm-3">
							<ul>
								<li>Fundraising</li>
								<li>How to Fundraising</li>
								<li>Schedule</li>
							</ul>
						</div>
						<div class="col-sm-3">
							<ul>
								<li>Event</li>
								<li>Guest Lists</li>
								<li>More</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="pull-right">
				lanjutkan
			</div>
		</div>
	</div>

<!-- modal -->
	<div id="email-modal" class="modal fade" role="dialog">
	  <div class="modal-dialog email-modal-wrapper">
		<button type="button" class="close" data-dismiss="modal">&times;</button>
		<div class="email-modal-content">
			<img class="img-part-1" src="<?=bloginfo('template_url')?>/assets/versi3/email_icon.png"/>
			<hr>
			<p>Masukkan email Anda dan berlangganan news letter dari kami untuk mendapatkan kabar terbaru mengenai ITB Homecoming Alumni 2015.</p>
			<div class="inner-addon right-addon input-email">
			 	<a href=""><span class="glyphicon glyphicon-play"></span></a>
			    <input type="text" class="form-control" placeholder="Email Anda" />
			</div>
		</div>
	  </div>
	</div>

	<!-- <div class="container">
		<div id="part1" class="part1">
			<div class="part1content--1">
	        	<img class="img1" src="<?=bloginfo('template_url')?>/assets/board1/logo_white.png"/>
	        </div>
	        <div class="part1content--2">
	        	Sekali teman, tetap teman
	        </div>
	        <div class="part1content--3">
	        	7 November 2015
	        </div>
	        <img src="<?=bloginfo('template_url')?>/assets/atas.jpg"/>
		</div>
		<div id="part2" class="part2">
			<div class="part2content--1">
	        	<img class="img2" src="<?=bloginfo('template_url')?>/assets/board3/Board 3-05.png"/>	
	        </div>
	        <div class="part2content--2">
	        	Mewadahi dan menjembatani seluruh alumni tanpa batas untuk turut serta membangun dan mengembangkan kampus ITB
	        </div>	
	        <div class="part2content--3">
	        	<img class="img3" src="<?=bloginfo('template_url')?>/assets/board2/Board 2-04.png"/>	
	        </div>
	        <div class="part2content--4">
	        	Sebuah dukungan untuk mewujudkan ITB sebagai World-Class University dan Entrepreneural University
	        </div>
			<img src="<?=bloginfo('template_url')?>/assets/tengah.jpg"/>	
		</div>
		<div class="part3">
			<div class="part3content--1">
	        	<img src="<?=bloginfo('template_url')?>/assets/board2/Board 2-14.png"/>	
	        </div>
	        <div class="part3content--2">
	        	Membuka pintu lebar kepulangan seluruh alumni untuk bernostalgia di kampus melalui berbagai acara kumpul dan acara-acara kesenian
	        </div>
	        <div id="googleMap">
	        </div>
	        <div class="part3content--3">
	        	Informasi lebih lanjut:<br>
	        	HUMAS ITB <br><blue>+62 812 2090 131</blue><br>
	        	SEKRETARIAT PRODI STEI <blue><br>+62 22 250 8135 / 250 8136</blue>
	        </div>
			<img src="<?=bloginfo('template_url')?>/assets/bawah.jpg"/>	
		</div>
	</div> -->
	<script type="text/javascript">
		$(function() {    
		var menuSlider = $('.menu-slider');

		$('.hamburger-menu').click(function(e) {
			e.preventDefault();
			if(!menuSlider.hasClass('active')) {
				menuSlider.addClass('active');
			} else {
				menuSlider.removeClass('active');
			}
		}); 
	});
	</script>
	<script>
		$(window).load(function(){
			var widthBG = $('.main-index').width();
			var heightBG = (1728/1600) * widthBG ;
			$('.main-index').css({'height':heightBG +'px'});
		});
		$(window).resize(function(){
			var widthBG = $('.main-index').width();
			var heightBG = (1728/1600) * widthBG ;
			$('.main-index').css({'height':heightBG +'px'});
		});
	</script>
	<script>
		var wid = $('.block1').width();
		var lef = wid/2;
		$( document ).ready(function() {
		    $(".block1").animate({ width: "0", left: "+="+lef }, 2000);
		});
	</script>
	<script type="text/javascript">
		function scrollToAnchor(aid){
		    var aTag = $("#"+aid);
		    $('html,body').animate({scrollTop: aTag.offset().top},'slow');
		}
		$(".arrow-down").click(function() {
		   scrollToAnchor('main');
		});
	</script>
	<script type="text/javascript">
    	$( document ).ready(function() {
		    $('#email-modal').modal('show');
		});
    </script>
</body>
</html>

<?php /* Template Name: Homecoming */ ?>
<html>
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <style>
    /* Prevents slides from flashing */
    #slides {
      display:none;
    }
  </style>

    <title>HOMECOMING ITB</title>
    <!-- Bootstrap Core CSS -->
    <link href="<?=bloginfo('template_url')?>/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <style type="text/css">
    	@font-face {
		  font-family: 'Trebuchet MS';
		  font-style: normal;
		  font-weight: 400;
		  src: local('Trebuchet MS'), local('TrebuchetMS'), url(http://fonts.gstatic.com/l/font?kit=9JQXGMjdcHmXcaI1rQ5rb_k_vArhqVIZ0nv9q090hN8&skey=7e071cef4f2cf8ce) format('woff2');
		}
		@font-face {
		  font-family: 'Trebuchet MS';
		  font-style: normal;
		  font-weight: 700;
		  src: local('Trebuchet MS Bold'), local('TrebuchetMS-Bold'), url(http://fonts.gstatic.com/l/font?kit=5ozLdgd-UkdFLWa7JSF1-rtgMuu8PP30JY7uijDhKnE&skey=3f0797581a987e40) format('woff2');
		}
		@font-face {
		  font-family: 'Trebuchet MS';
		  font-style: italic;
		  font-weight: 400;
		  src: local('Trebuchet MS Italic'), local('TrebuchetMS-Italic'), url(http://fonts.gstatic.com/l/font?kit=lNFXl5wenEwSNlIU9g-Z3B8g4Wdd7hYU3Jvl4elC6n4&skey=a5a066eb747abea4) format('woff2');
		}
		@font-face {
		  font-family: 'Trebuchet MS';
		  font-style: italic;
		  font-weight: 700;
		  src: local('Trebuchet MS Bold Italic'), local('Trebuchet-BoldItalic'), url(http://fonts.gstatic.com/l/font?kit=d8kxGwlaqsR0T3Chlg-X6mceCCgISh9AIMdEVQO6ryk&skey=8ff479c3862a8342) format('woff2');
		}
		body {
		    font-family: 'Trebuchet Ms';
		    font-size: 200%;
		}
    </style>

    <link href="<?=bloginfo('template_url')?>/style.css" rel="stylesheet">
    <script src="<?=bloginfo('template_url')?>/js/jquery-1.11.3.min.js"></script>
    <script src="<?=bloginfo('template_url')?>/js/bootstrap.min.js"></script>
    <script type="text/javascript">
    	function scrollToAnchor(offset){
		    //var aTag = $(aid);
		    $('html,body').animate({scrollTop: offset},'slow');
		}
    </script>
    <script src="http://maps.googleapis.com/maps/api/js"></script>
    <script>
	function initialize() {
	  var mapProp = {
	    center:new google.maps.LatLng(-6.8920,107.6100),
	    zoom:15,
	    mapTypeId:google.maps.MapTypeId.ROADMAP
	  };
	  var map=new google.maps.Map(document.getElementById("googleMap"), mapProp);
	}
	google.maps.event.addDomListener(window, 'load', initialize);
	</script>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
	<div class="fluid-container">
		<?php
			$args = array (
							'post_type' => 'post',
							'post_status' => 'publish',
							'showposts' => -1,
							'category_name' => 'Homecoming',
							'order' => 'ASC'
						);
			$my_query = null;
			$my_query = new WP_Query($args);
		?>
		<?php if ( $my_query->have_posts() ) : while ( $my_query->have_posts() ) : $my_query->the_post(); ?>
					<div class="part">
				        <div class="title-post">
				        	<?php echo get_the_title(); ?>
				        </div>
				        <?php $categories = get_the_category(); ?>
				        	<?php if(($categories[1]->cat_name == 'Right')||($categories[0]->cat_name == 'Right')){?>
				        		<div class="text-right-image"><?php echo get_the_content(); ?></div>
				        	<?php } ?>
				        	<?php if(($categories[1]->cat_name == 'Center')||($categories[0]->cat_name == 'Center')){?>
				        		<div class="text-center-image"><?php echo get_the_content(); ?></div>
				        	<?php } ?>
				        	<?php if(($categories[1]->cat_name == 'Left')||($categories[0]->cat_name == 'Left')){?>
				        		<div class="text-left-image"><?php echo get_the_content(); ?></div>
				        	<?php } ?>
				        <?php the_post_thumbnail(); ?>
					</div>
		<?php endwhile; else : ?>
			<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
		<?php endif; ?>
	</div>
</body>
</html>